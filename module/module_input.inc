<?php

/**
 * addon_r3_newsblogs
 * @author t.schmidt[at]raum3[dot]at Thomas Schmidt
 */

// module:addon_r3_newsblogs_basic_input

?>
<strong>News, oder Blog?</strong>
<br/>
<select name="REX_INPUT_VALUE[1]">
	<option value="News" <?php echo ("REX_VALUE[1]" == 'News') ? 'selected' : ''; ?>>News</option>
	<option value="Blog" <?php echo ("REX_VALUE[1]" == 'Blog') ? 'selected' : ''; ?>>Blog</option>
</select>
<br/>
<br/>
<strong>Liste, oder Archiv?</strong>
<br/>
<select name="REX_INPUT_VALUE[2]">
	<option value="Liste" <?php echo ("REX_VALUE[2]" == 'Liste') ? 'selected' : ''; ?>>Liste</option>
	<option value="Archiv" <?php echo ("REX_VALUE[2]" == 'Archiv') ? 'selected' : ''; ?>>Archiv</option>
</select>
<br/>
<br/>
<strong>Anzahl der auszugebenden Elemente</strong>
<br/>
<select name="REX_INPUT_VALUE[3]">
	<option value="" <?php echo ("REX_VALUE[3]" == '') ? 'selected' : ''; ?>>--- Alle ---</option>
	<option value="3" <?php echo ("REX_VALUE[3]" == '3') ? 'selected' : ''; ?>>3</option>
	<option value="6" <?php echo ("REX_VALUE[3]" == '6') ? 'selected' : ''; ?>>6</option>
	<option value="9" <?php echo ("REX_VALUE[3]" == '9') ? 'selected' : ''; ?>>9</option>
	<option value="12" <?php echo ("REX_VALUE[3]" == '12') ? 'selected' : ''; ?>>12</option>
</select>
