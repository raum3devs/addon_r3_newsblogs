<?php

/**
 * addon_r3_newsblogs
 * @author t.schmidt[at]raum3[dot]at Thomas Schmidt
 */

// module:addon_r3_newsblogs_basic_output

$zeige = "REX_VALUE[1]";
$form = "REX_VALUE[2]";
$anzahl = "REX_VALUE[3]";

if(rex::isBackend()){
	echo '<h3>'.$zeige.'</h3><hr/>';
}

?>

<div class="<?php echo $zeige.'_'.$form; ?>_wrapper">
	<?php echo newsblogs::renderOutput($zeige, $form, $anzahl); ?>
</div>