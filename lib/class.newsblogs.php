<?php

class newsblogs {
	
	public static function changeStatus($articleID = '', $currentToggle = '', $what = ''){
		if($articleID != '' && $what != ''){
			$sql = rex_sql::factory();
			
			$query = "UPDATE ".rex::getTable('article')." SET r3nb_$what = $currentToggle WHERE id = $articleID";
			$sql->setQuery($query);
			
		}
	}
	
	public static function renderOutput($show = '', $form = '', $anzahl = ''){
		if($show != ''){
			switch($show){
				case 'News':
					$dataArr = self::getData('r3nb_news', $anzahl);
					switch($form){
						case 'Liste':
							return self::getNewsListOutput($dataArr);
							break;
						case 'Archiv':
							return self::getNewsArchiveOutput($dataArr);
							break;
					}
					break;
				case 'Blog':
					$dataArr = self::getData('r3nb_blog', $anzahl);
					switch($form){
						case 'Liste':
							return self::getBlogListOutput($dataArr);
							break;
						case 'Archiv':
							return self::getBlogArchiveOutput($dataArr);
							break;
					}
					break;
			}
		}
	}
	
	private static function getNewsListOutput($dataArr){
		$out = '<div class="container-fluid">';
		
		if(count($dataArr) > 0){
			$anz = count($dataArr);
			$i = 0;
			
			foreach($dataArr as $res){
				$i++;
				
				$img = '';
				if($res['r3nb_image'] != ''){
					if(rex_media::get($res['r3nb_image'])->isImage()){
						$img = '<img src="index.php?rex_media_type=r3newsblogs_fe&amp;rex_media_file='.$res['r3nb_image'].'" />';
					}
				}
				
				$out .= '
						<div class="news_element col-md-4 col-sm-12">
							<div class="news_date">'.self::parseDate($res['r3nb_date_from'], $res['r3nb_date_to']).'</div>
							
							
							<div class="news_content">
								<div class="news_image pull-left"><a href="'.rex_getUrl($res['id']).'">'.$img.'</a></div>
								<div class="news_headline"><a href="'.rex_getUrl($res['id']).'">'.$res['r3nb_title'].'</a></div>
								<div class="news_text"><p>'.nl2br($res['r3nb_shortdescription']).'</p></div>
								<div class="news_more"><a href="'.rex_getUrl($res['id']).'">Lesen Sie mehr <i class="fa fa-arrow-circle-right"></i></a></div>
							</div>
						</div>
						';
				#$out .= '
				#		<div class="news_element">
				#			<div class="news_image"><a href="'.rex_getUrl($res['id']).'">'.$img.'</a></div>
				#			<div class="news_content">
				#				<div class="news_date">'.self::parseDate($res['r3nb_date_from'], $res['r3nb_date_to']).'</div>
				#				<div class="news_headline"><a href="'.rex_getUrl($res['id']).'">'.$res['r3nb_title'].'</a></div>
				#				<div class="news_text"><p>'.nl2br($res['r3nb_shortdescription']).'</p></div>
				#				<div class="news_more"><a href="'.rex_getUrl($res['id']).'">Lesen Sie mehr <i class="fa fa-arrow-circle-right"></i></a></div>
				#			</div>
				#		</div>
				#		';
				if($i%3 == 0 && $i != $anz){
					$out .= '<div class="clearfix"></div></div><div class="container-fluid">';
				}
			}
		}
		
		return $out.'<div class="clearfix"></div></div>';
	}
	
	private static function getNewsArchiveOutput($dataArr){
		$out = '';
		
		if(count($dataArr) > 0){
			foreach($dataArr as $res){
				$out .= '<div class="news_archive_element">
							<a href="'.rex_getUrl($res['id']).'">
								<span class="news_archive_date">'.self::parseDate($res['r3nb_date_from'], $res['r3nb_date_to'], $res['id']).'</span>
								<span class="news_archive_headline">| '.$res['r3nb_title'].'</span>
								<span class="news_archive_read_more pull-right">'.rex_i18n::msg('addon_r3_newsblogs_read_more').' <i class="fa fa-arrow-circle-right"></i></span>
							</a>
						</div>';
			}
		}
		
		return $out;
	}
	
	private static function getBlogListOutput($dataArr){
		$out = '';
		
		if(count($dataArr) > 0){
			foreach($dataArr as $res){
				
				$img = '';
				if($res['r3nb_image'] != ''){
					if(rex_media::get($res['r3nb_image'])->isImage()){
						$img = '<img src="index.php?rex_media_type=r3newsblogs_fe&amp;rex_media_file='.$res['r3nb_image'].'" />';
					}
				}
				
				$out .= '
						<div class="blog_element">
							<div class="blog_image"><a href="'.rex_getUrl($res['id']).'">'.$img.'</a></div>
							<div class="blog_content">
								<div class="blog_date">'.self::parseDate($res['r3nb_date_from'], $res['r3nb_date_to']).'</div>
								<div class="blog_headline"><a href="'.rex_getUrl($res['id']).'">'.$res['r3nb_title'].'</a></div>
								<div class="blog_text"><p>'.nl2br($res['r3nb_shortdescription']).'</p></div>
								<div class="blog_more"><a href="'.rex_getUrl($res['id']).'">Lesen Sie mehr <i class="fa fa-arrow-circle-right"></i></a></div>
							</div>
						</div>
						';
			}
		}
		return $out;
	}
	
	private static function getBlogArchiveOutput($dataArr){
		$out = '';
		
		if(count($dataArr) > 0){
			foreach($dataArr as $res){
				$out .= '<div class="blog_archive_element">
							<a href="'.rex_getUrl($res['id']).'">
								<span class="news_archive_date">'.self::parseDate($res['r3nb_date_from'], $res['r3nb_date_to'], $res['id']).'</span>
								<span class="news_archive_headline">| '.$res['r3nb_title'].'</span>
								<span class="news_archive_read_more pull-right">'.rex_i18n::msg('addon_r3_newsblogs_read_more').' <i class="fa fa-arrow-circle-right"></i></span>
							</a>
						</div>';
			}
		}
		
		return $out;
	}
	
	public static function getData($field = '', $anzahl = ''){
		$date = date('Y-m-d');
		$sql = rex_sql::factory();
		
		if($anzahl != ''){
			$limit = 'LIMIT '.$anzahl;
		}else{
			$limit = '';
		}
		
		$qry = "SELECT id, r3nb_image, r3nb_title, r3nb_shortdescription, r3nb_date_from, r3nb_date_to FROM ".rex::getTable('article')." WHERE $field = 1 AND r3nb_title != '' AND (r3nb_daterange = 0 OR (r3nb_daterange = 1 AND r3nb_date_from >= $date  AND r3nb_date_to <= $date)) ORDER BY r3nb_date_from DESC ".$limit;
		$sql->setQuery($qry);
		
		return $sql->getArray();
	}
	
	public static function parseDate($from = '', $to = ''){
		$date = '';
		if($from != '' && $to != ''){
			if($from == $to){
				$date = date_format(date_create($from), 'd.m.Y');
			}elseif($from > $to){
				$date = date_format(date_create($to), 'd.m.Y').' - '.date_format(date_create($from), 'd.m.Y');;
			}else{
				$date = date_format(date_create($from), 'd.m.Y').' - '.date_format(date_create($to), 'd.m.Y');;
			}
		}
		return $date;
	}
	
	public static function postSave($params){
		$value_pool = $params['form']->getParam('value_pool');
		if(
		   isset($value_pool['sql']['r3nb_date_from']) AND
		   isset($value_pool['sql']['r3nb_date_to'])
		) {
			$date_from = $value_pool['sql']['r3nb_date_from'];
			$date_to = $value_pool['sql']['r3nb_date_to'];

			$table = $params['form']->getElement(2);

			$id = explode(' ',$params['form']->getElement(3));
			$id = explode('=', $id[0]);

			if($date_from > $date_to){
				$sql = rex_sql::factory();
				$sql->setQuery("UPDATE $table SET r3nb_date_from = '$date_to', r3nb_date_to = '$date_from' WHERE id= ".$id[1]);
			}
		}
	}
	
	public static function addArticle($postData){
		$sql = rex_sql::factory();
		
		$return = '';
		
		$typ = $postData['type'];
		$prio = $postData['article-position'];
		
		switch($typ){
			case 'news':
				$parent_id = rex_package::get('addon_r3_newsblogs')->getConfig('addon_r3_newsblogs_path_news');
				break;
			case 'blog':
				$parent_id = rex_package::get('addon_r3_newsblogs')->getConfig('addon_r3_newsblogs_path_blogs');
				break;
		}
		
		if($prio == ''){
			$prio = 2;
			
			if($parent_id != ''){
				$sql->setQuery("SELECT priority FROM ".rex::getTable('article')." WHERE parent_id = $parent_id ORDER BY priority DESC LIMIT 1");
				$result = $sql->getArray();
				
				if(count($result) > 0){
					foreach($result as $res){
						if($res['priority'] > $prio){
							$prio = $res['priority'];
						}
					}
					$prio++;
				}
			}
		}
		
		$data = [];
		$data['name'] = $postData['article-name'];
		$data['priority'] = $prio;
		$data['template_id'] = $postData['template-id'];
		$data['category_id'] = $parent_id;
		
		$addArt = new rex_api_result(true, rex_article_service::addArticle($data));
		
		$sql->setQuery("SELECT id FROM ".rex::getTable('article')." WHERE name = '".$postData['article-name']."' ORDER BY id DESC LIMIT 1");
		$result = $sql->getArray();
		
		if(count($result) > 0){
			$article_id = $result[0]['id'];
		}else{
			$article_id = 0;
		}
		
		return array($typ, $addArt->isSuccessfull(), $article_id);
	}
	
	public static function addJStoBackend($package) {
		if( rex_get('page') == $package.'/overview' ) {
			
			
			rex_extension::register('PAGE_HEADER', function(rex_extension_point $ep) {
				$content = $ep->getSubject();
				$content = '<script src="'.rex_url::addonAssets($ep->getParam('package'), 'js/jquery.tablesorter.min.js').'" type="text/javascript"></script>';
				$ep->setSubject($content);
			}, 'NORMAL', array('package' => $package));
		}
	}
}

?>