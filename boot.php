<?php

if(rex::isBackend()){
	
	rex_extension::register('REX_YFORM_SAVED', function (rex_extension_point $ep) {
		$params = $ep->getParams();
		$params['subject'] = $ep->getSubject();
		$params['extension_point'] = $ep->getName();
		$params = newsblogs::postSave($params);
	}, rex_extension::EARLY);
	
	rex_extension::register('STRUCTURE_CONTENT_SIDEBAR', function (rex_extension_point $ep) {
		$params = $ep->getParams();
		$subject = $ep->getSubject();

		$panel = include(rex_path::addon('addon_r3_newsblogs','pages/content.sidebar.php'));

		$fragment = new rex_fragment();
		$fragment->setVar('title', '<i class="rex-icon rex-icon-info"></i> ' . rex_i18n::msg('addon_r3_newsblogs_sidetitle'), false);
		$fragment->setVar('body', $panel, false);
		$fragment->setVar('article_id', $params["article_id"], false);

		$fragment->setVar('collapse', true);
		$fragment->setVar('collapsed', false);
		$content = $fragment->parse('core/page/section.php');

		return $subject.$content;

	});
	
	newsblogs::addJStoBackend(rex_package::getName());
	
}else{
	
}

?>