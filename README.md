Das News/Blog Addon erweitert die Seitenleiste eines Artikels und erweitert diesen um folgende Parameter:

* **News-Element** - Bestimmt ob dieser Artikel als News-Element in der News-Ausgabe dargestellt wird
* **Blog-Element** - Bestimmt ob dieser Artikel als Blog-Element in der Blog-Ausgabe dargestellt wird
* **Titel** - Ist der Titel der dargestellten Kachel
* **Kurzbeschreibung** - Ist der Teasertext der dargestellten Kachel
* **Datum von** - Setzt das ausgegebene "von"-Datum in der News/Blog-Ausgabe
* **bis** - Setzt das ausgegebene "bis"-Datum in der News/Blog-Ausgabe
* **Nur in ausgewähltem Zeitraum anzeigen** - Bestimmt, ob das Element nur im eingestellten Zeitraum angezeigt wird, oder immer 

---

<p class="danger-alert">
	Ein Element wird in der Ausgabe nur angezeigt, wenn eine Auswahl bezüglich des Typs getroffen (News und/oder Blog) und eine Headline eingetragen wurde.
</p>

---
## Funktionsweise:

Beim Erstellen eines Artikels wird die Sidebar (rechts) um eine Sektion erweitert.

In dieser Sektion werden alle notwendigen Parameter (siehe oben) für die Ausgabe gesetzt.

In den **Einstellungen** können Kategorien gewählt werden, wodurch man dann in der **Übersicht** in der Lage ist weitere Artikel in diesen Kategorien anzulegen.

In der **Überischt** sieht man alle Informationen die in den jeweiligen Artikel gesetzt worden sind.
Dort lassen sich ebenfalls die Stati der Artikel ändern.



<style type="text/css">
	.danger-alert{
		color: #a94442;
		background-color: #f2dede;
		border: 1px solid #ebccd1;
		border-radius: 4px;
		padding: 15px;
	}
</style>