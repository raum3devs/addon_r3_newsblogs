<?php

rex_sql_table::get(rex::getTable('article'))
    ->removeColumn('r3nb_news')
    ->removeColumn('r3nb_blog')
    ->removeColumn('r3nb_image')
    ->removeColumn('r3nb_title')
    ->removeColumn('r3nb_shortdescription')
    ->removeColumn('r3nb_daterange')
    ->removeColumn('r3nb_date_from')
    ->removeColumn('r3nb_date_to')
    ->alter()
;

$sql = rex_sql::factory();
$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_be'");
$result = $sql->getArray();
if(count($result) > 0){
	$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_be'");
	$type_id = $sql->getValue('id');
	
	$sql->setQuery("DELETE FROM ".rex::getTable('media_manager_type')." WHERE id = $type_id");
	$sql->setQuery("DELETE FROM ".rex::getTable('media_manager_type_effect')." WHERE type_id = $type_id");
}

$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_fe'");
$result = $sql->getArray();
if(count($result) > 0){
	$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_fe'");
	$type_id = $sql->getValue('id');
	
	$sql->setQuery("DELETE FROM ".rex::getTable('media_manager_type')." WHERE id = $type_id");
	$sql->setQuery("DELETE FROM ".rex::getTable('media_manager_type_effect')." WHERE type_id = $type_id");
}

?>