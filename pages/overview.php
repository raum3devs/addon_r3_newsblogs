<?php
#echo rex_view::title($this->i18n('title'));

$func= rex_get('func', 'string', '');
$articleID = rex_get('aid', 'integer', '');

$newsState = rex_get('newsState', 'integer', '');
$blogState = rex_get('blogState', 'integer', '');
$dateState = rex_get('dateState', 'integer', '');

$type = rex_request('type', 'string', '');

$buttonContent = '';
$listTable = '';

$addArticleFormNews = '';

$formErr = false;
if($func == 'addArticle'){
	if(empty($_POST['article-name'])){
		$func = 'showForm';
		$formErr = true;
	}
}
switch($func){

	case 'news':
		newsblogs::changeStatus($articleID, $newsState, 'news');
		break;
	case 'blog':
		newsblogs::changeStatus($articleID, $blogState, 'blog');
		break;
	case 'date':
		newsblogs::changeStatus($articleID, $dateState, 'daterange');
		break;
	case 'showForm':
		$sql2 = rex_sql::factory();
		$sql2->setQuery('SELECT id, name FROM ' . rex::getTable('template') . ' WHERE active=1');
		$templates = $sql2->getArray();
		
		if(isset($_POST['article-name']) && empty($_POST['article-name'])){
			$articleName = '';
			$articleNameError = 'has-error';
		}else{
			$articleName = '';
			$articleNameError = '';
		}
		
		if(count($templates) > 0){
			$templateSelect = '<select name="template-id" class="form-control">';
			foreach($templates as $template){
				$templateSelect .= '<option value="'.$template['id'].'">'.$template['name'].'</option>';
			}
			$templateSelect .= '</select>';
			
			$addArticleFormNews .= '<br/>';
			$addArticleFormNews .= '<div class="col-md-12" ><strong>'.$this->i18n('addon_r3_newsblogs_add_article_'.$type).'</strong></div>';
			$addArticleFormNews .= '<form action="'.rex_url::currentBackendPage(array('func' => 'addArticle')).'" method="post">
										<div class="col-md-3 '.$articleNameError.'">
											<input class="form-control" type="text" name="article-name" placeholder="'.$this->i18n('addon_r3_newsblogs_page_name').'" value="" autofocus />
										</div>
										<div class="col-md-3">
											'.$templateSelect.'
										</div>
										<div class="col-md-3">
											<input class="form-control" type="text" name="article-position" placeholder="Prio" value="" />
										</div>
										<div class="col-md-3">
											<input type="hidden" name="type" value="'.$type.'" />
											<button class="btn btn-save" type="submit" name="artadd_function"' . rex::getAccesskey(rex_i18n::msg('article_save'), 'save') . '>' . rex_i18n::msg('article_save') . '</button>
										</div>
									</form>';
		}else{
			$addArticleFormNews .= '<div>'.rex_i18n::msg('addon_r3_newsblogs_add_template').'</div>';
		}
		if($formErr){
			
		}
		break;
	case 'addArticle':
		
		list($typ, $success, $article_id) = newsblogs::addArticle($_POST);
		if($success){
			$buttonContent .= rex_view::info($this->i18n('addon_r3_article_added_'.$typ).'<br/><a href="index.php?page=content/edit&amp;article_id='.$article_id.'&amp;mode=edit">'.$this->i18n('addon_r3_article_edit_article').'</a>');
		}else{
			$buttonContent .= rex_view::warning($this->i18n('addon_r3_article_added_error'.$typ));
		}
		break;
}

$r3nb_path_news = $this->getConfig('addon_r3_newsblogs_path_news');
$r3nb_path_blogs = $this->getConfig('addon_r3_newsblogs_path_blogs');

if($r3nb_path_news != ''){
	#$buttonContent .= '<div class="col-md-6"><a href="'.rex_url::currentBackendPage(array('func' => 'addArticle', 'type' => 'news')).'" role="button" class="btn btn-primary">'.$this->i18n('addon_r3_newsblogs_add_article_news').'</a></div>';
	$buttonContent .= '<div class="col-md-6">
							<a href="index.php?page=structure&amp;category_id='.$r3nb_path_news.'&amp;article_id=0">'.$this->i18n('addon_r3_newsblogs_to_news_articles').'</a>
							<br/>
							<br/>
							<a href="'.rex_url::currentBackendPage(array('func' => 'showForm', 'type' => 'news')).'" role="button" class="btn btn-primary">'.$this->i18n('addon_r3_newsblogs_add_article_news').'</a>
						</div>';
}
if($r3nb_path_blogs != ''){
	$buttonContent .= '<div class="col-md-6">
							<a href="index.php?page=structure&amp;category_id='.$r3nb_path_blogs.'&amp;article_id=0">'.$this->i18n('addon_r3_newsblogs_to_blog_articles').'</a>
							<br/>
							<br/>
							<a href="'.rex_url::currentBackendPage(array('func' => 'showForm', 'type' => 'blog')).'" role="button" class="btn btn-primary">'.$this->i18n('addon_r3_newsblogs_add_article_blog').'</a>
						</div>';
}
if($buttonContent != ''){
	$buttonContent .= '<div class="clearfix"></div>';
}else{
	$buttonContent .= $this->i18n('addon_r3_newsblogs_add_articles_info');
}


$fragment = new rex_fragment();
$fragment->setVar('title', $this->i18n('addon_r3_newsblogs_add_articles'));
$fragment->setVar('body', $buttonContent.$addArticleFormNews, false);
echo $fragment->parse('core/page/section.php');



$sql = rex_sql::factory();

$sql->setQuery("SELECT * FROM ".rex::getTable('article').' WHERE r3nb_title != "" ORDER BY r3nb_date_from DESC');
$result = $sql->getArray();

#echo "<pre>";
#print_r($result);
#echo "</pre>";

$clangArr = rex_clang::getAll(true);

#echo "<pre>";
#print_r($clangArr);
#echo "</pre>";

if(count($clangArr) > 1){
	$listTable .= '<ul class="nav nav-tabs" role="tablist">';
	foreach($clangArr as $clang){
		if(rex_clang::getCurrentId() == $clang->getId()){
			$act = 'active';
		}else{
			$act = '';
		}
		$listTable .= '<li role="presentation" class="'.$act.'"><a href="#'.$clang->getName().'" role="tab" data-toggle="tab">'.$clang->getName().'</a></li>';
	}
	$listTable .= '</ul>';
}


$addonPath = rex_package::getName().'/'.rex_be_controller::getCurrentPagePart(2);

$rowArr = array();

foreach($result as $res){
	
	switch($res['r3nb_news']){
		case '1':
			$news = '<span class="label label-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>';
			$newsState = '0';
			break;
		default:
			$news = '<span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>';
			$newsState = '1';
	}
	switch($res['r3nb_blog']){
		case '1':
			$blog = '<span class="label label-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>';
			$blogState = '0';
			break;
		default:
			$blog = '<span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>';
			$blogState = '1';
	}
	switch($res['r3nb_daterange']){
		case '1':
			$daterange = '<span class="label label-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></span>';
			$dateState = '0';
			break;
		default:
			$daterange = '<span class="label label-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></span>';
			$dateState = '1';
	}
	
	$img = '';
	if($res['r3nb_image'] != ''){
		if(rex_media::get($res['r3nb_image'])->isImage()){
			$img = '<div class="listImage"><img src="index.php?rex_media_type=r3newsblogs_be&amp;rex_media_file='.$res['r3nb_image'].'" /></div>';
		}
	}
	
	$rowArr[rex_clang::get($res['clang_id'])->getName()][] = '	<tr>
																	<td>'.$res['id'].'</td>
																	<td><a href="index.php?page=content/edit&amp;article_id='.$res['id'].'&amp;mode=edit">'.$res['name'].'</a></td>
																	<td><a href="index.php?page='.$addonPath.'&amp;func=news&amp;newsState='.$newsState.'&amp;aid='.$res['id'].'">'.$news.'</a></td>
																	<td><a href="index.php?page='.$addonPath.'&amp;func=blog&amp;blogState='.$blogState.'&amp;aid='.$res['id'].'">'.$blog.'</a></td>
																	<!--<td>'.$img.'</td>
																	<td>'.$res['r3nb_title'].'</td>
																	<td class="descr">'.$res['r3nb_shortdescription'].'</td>-->
																	<td>'.$res['r3nb_date_from'].'</td>
																	<td>'.$res['r3nb_date_to'].'</td>
																	<td><a href="index.php?page='.$addonPath.'&amp;func=date&amp;dateState='.$dateState.'&amp;aid='.$res['id'].'">'.$daterange.'</a></td>
																	<td><button type="button" class="btn btn-default" data-toggle="popover" title="'.$res['r3nb_title'].'" data-content="'.$res['r3nb_shortdescription'].'" data-placement="top" data-container="body">'.rex_i18n::msg('addon_r3_newsblogs_show_content').'</button></td>
																</tr>';
}


#echo "<pre>";
#print_r($rowArr);
#echo "</pre>";

$listTable .= '<div class="tab-content">';

	foreach($clangArr as $clang){
		if(rex_clang::getCurrentId() == $clang->getId()){
			$act = 'active';
		}else{
			$act = '';
		}
		$listTable .= '<div role="tabpanel" class="tab-pane fade in '.$act.'" id="'.$clang->getName().'">';
		
			$listTable .= '<table id="overviewList_'.$clang->getName().'" class="table table-striped table-hover table-condensed tablesorter">
							<thead>
								<tr>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_page_id').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_page_name').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_news').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_blog').'</th>
									<!--<th>'.rex_i18n::msg('addon_r3_newsblogs_side_image').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_title').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_description').'</th>-->
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_date_from').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_date_to').'</th>
									<th>'.rex_i18n::msg('addon_r3_newsblogs_side_daterange').'</th>
								</tr>
							</thead>
							<tbody>';
			
				$listTable .= implode('', $rowArr[$clang->getName()]);
			$listTable .= '	</tbody>
						  </table>';
						  
		$listTable .= '</div>';
	}
	

$listTable .= '</div>';

$content = '<div>
				<div class="col-md-6 col-"></div>
			</div>
			<p>
				'.$listTable.'
			</p>';

$fragment = new rex_fragment();
$fragment->setVar('title', $this->i18n('addon_r3_newsblogs_articles'));
$fragment->setVar('body', $content, false);
echo $fragment->parse('core/page/section.php');




#$content = '';
#$fragment = new rex_fragment();
#$fragment->setVar('title', $this->i18n('less_compiler_erklaerung_title'));
#$fragment->setVar('body', $content, false);
#echo $fragment->parse('core/page/section.php');

?>
<style type="text/css">
	td.descr{
		max-width: 80px;
		word-wrap: break-word;
	}
	th.header{
		vertical-align: middle !important;
		padding-right: 22px !important;
		background-image: url('<?php echo rex_url::addonAssets(rex_package::getName(), 'img/bg.gif'); ?>');
		background-repeat: no-repeat;
		background-position: center right;
		cursor: pointer;
	}
	th.headerSortDown{
		background-image: url('<?php echo rex_url::addonAssets(rex_package::getName(), 'img/desc.gif'); ?>');
	}
	th.headerSortUp{
		background-image: url('<?php echo rex_url::addonAssets(rex_package::getName(), 'img/asc.gif'); ?>');
	}
	.popover-title{
		font-weight: bold;
	}
	.popover-content{
		word-wrap: break-word;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		<?php
			foreach($clangArr as $clang){
				echo "$('#overviewList_".$clang->getName()."').tablesorter();";
			}
		?>
		
		$('[data-toggle="popover"]').popover();
	});
</script>