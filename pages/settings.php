<?php
if (rex::getUser()->isAdmin()) {

    $content = '';
    $searchtext = 'module:addon_r3_newsblogs_basic_output';

    $gm = rex_sql::factory();
    $gm->setQuery('select * from rex_module where output LIKE "%' . $searchtext . '%"');

    $module_id = 0;
    $module_name = '';
    foreach ($gm->getArray() as $module) {
        $module_id = $module['id'];
        $module_name = $module['name'];
    }

    $r3nb_module_name = 'Raum3 News/Blog Output';
	
	$addonPath = rex_package::getName().'/'.rex_be_controller::getCurrentPagePart(2);

    if (rex_request('install',"integer") == 1) {

        $input = rex_file::get(rex_path::addon('addon_r3_newsblogs','module/module_input.inc'));
        $output = rex_file::get(rex_path::addon('addon_r3_newsblogs','module/module_output.inc'));

        $mi = rex_sql::factory();
        // $mi->debugsql = 1;
        $mi->setTable('rex_module');
        $mi->setValue('input', $input);
        $mi->setValue('output', $output);

        if ($module_id == rex_request('module_id','integer',-1)) {
            $mi->setWhere('id="' . $module_id . '"');
            $mi->update();
            echo rex_view::success('Modul "' . $module_name . '" wurde aktualisiert');

        } else {
            $mi->setValue('name', $r3nb_module_name);
            $mi->insert();
            $module_id = (int) $mi->getLastId();
            $module_name = $r3nb_module_name;
            echo rex_view::success('News/Blog Modul wurde angelegt unter "' . $r3nb_module_name . '"');
        }

    }
    if ($module_id > 0) {
        $content .= '<p><a class="btn btn-primary" href="index.php?page='.$addonPath.'&amp;install=1&amp;module_id=' . $module_id . '" class="rex-button">' . $this->i18n('install_update_module', htmlspecialchars($module_name)) . '</a></p>';

    }else {
        $content .= '<p><a class="btn btn-primary" href="index.php?page='.$addonPath.'&amp;install=1" class="rex-button">' . $this->i18n('install_addon_r3_newsblogs_modul', $r3nb_module_name) . '</a></p>';

    }

    $fragment = new rex_fragment();
    $fragment->setVar('title', $this->i18n('install_modul'), false);
    $fragment->setVar('body', $content , false);
    echo $fragment->parse('core/page/section.php');
	
	/*************************************************************************************************/
	
	$content = '';
	
	if (rex_post('config-submit', 'boolean')) {
		$this->setConfig(
			rex_post('config', [
				['addon_r3_newsblogs_path_news', 'string'],
				['addon_r3_newsblogs_path_blogs', 'string'],
			]
		));

		$content .= rex_view::info($this->i18n('addon_r3_newsblogs_saves'));
	}

	$r3nb_save_btn	= $this->i18n('addon_r3_newsblogs_save');
	
	$r3nb_path_news = $this->getConfig('addon_r3_newsblogs_path_news');
	$r3nb_path_blogs = $this->getConfig('addon_r3_newsblogs_path_blogs');
	
	
	#$categoryArr = newsblogs::getCategoryArray(true, rex_clang::getCurrentId());
	
	$select_news = new rex_category_select(true, rex_clang::getCurrentId(), false, true);
	$select_news->setAttribute('name', 'config[addon_r3_newsblogs_path_news]');
	$select_news->setAttribute('class', 'form-control');
	$select_news->addOption($this->i18n('addon_r3_newsblogs_select_category'), '');
	$select_news->setSelected($r3nb_path_news);
	
	$select_blog = new rex_category_select(true, rex_clang::getCurrentId(), false, true);
	$select_blog->setAttribute('name', 'config[addon_r3_newsblogs_path_blogs]');
	$select_blog->setAttribute('class', 'form-control');
	$select_blog->addOption($this->i18n('addon_r3_newsblogs_select_category'), '');
	$select_blog->setSelected($r3nb_path_blogs);
	
	$content .= '
		<form action="'.rex_url::currentBackendPage().'" method="post" id="addon_r3_newsblogs_setting">
			<div class="container-fluid">

				<div class="col-xs-12">
					<label>'.$this->i18n("addon_r3_newsblogs_path_news").'</label>
					'.$select_news->get().'
					<br />
					
					<label>'.$this->i18n("addon_r3_newsblogs_path_blogs").'</label>
					'.$select_blog->get().'
				</div>
				
				<div class="col-xs-12">
					<hr />
					<button class="btn btn-save" type="submit" name="config-submit" value="1" title="'.$r3nb_save_btn.'">'.$r3nb_save_btn.'</button>
				</div>

			</div>
		</form>
	';

	$fragment = new rex_fragment();
	$fragment->setVar('class', 'edit');
	$fragment->setVar('title', $this->i18n('addon_r3_newsblogs_settings'));
	$fragment->setVar('body', $content, false);
	echo $fragment->parse('core/page/section.php');
	
}
?>