<?php

$addon = rex_addon::get('addon_r3_newsblogs');

$article_id = $params['article_id'];
$clang = $params['clang'];
$ctype = $params['ctype'];

$yform = new rex_yform();
$yform->setObjectparams('form_action', rex_url::backendController(['page' => 'content/edit', 'article_id' => $article_id, 'clang' => $clang, 'ctype' => $ctype], false));
$yform->setObjectparams('form_showformafterupdate', 1);
$yform->setObjectparams('main_table', rex::getTable('article'));
$yform->setObjectparams('main_id', $article_id);
$yform->setObjectparams('main_where', 'id='.$article_id.' and clang_id='.$clang);
$yform->setObjectparams('getdata', true);

$yform->setValueField('html', array('', '<div><strong><span style="color: red;">*</span>Anzeigen als:</strong></div>'));

$yform->setValueField('checkbox', array('r3nb_news', rex_i18n::msg('addon_r3_newsblogs_side_news')));
$yform->setValueField('checkbox', array('r3nb_blog', rex_i18n::msg('addon_r3_newsblogs_side_blog')));

$yform->setValueField('html', array('', '<hr/>'));

$yform->setValueField('be_media', array('r3nb_image', rex_i18n::msg('addon_r3_newsblogs_side_image')));

$yform->setValueField('text', array('r3nb_title', '<span style="color: red;">*</span>'.rex_i18n::msg('addon_r3_newsblogs_side_title')));
$yform->setValueField('textarea', array('r3nb_shortdescription', rex_i18n::msg('addon_r3_newsblogs_side_description')));

$yform->setValueField('html', array('', '<hr/>'));

$yform->setValueField('text', array('r3nb_date_from', rex_i18n::msg('addon_r3_newsblogs_side_date_from'),'','', array('class' => 'form-control datepicker')));
$yform->setValueField('text', array('r3nb_date_to', rex_i18n::msg('addon_r3_newsblogs_side_date_to'),'','', array('class' => 'form-control datepicker2')));

$yform->setValueField('checkbox', array('r3nb_daterange', rex_i18n::msg('addon_r3_newsblogs_side_daterange')));

#$yform->setHiddenField('r3nb_date_from', '');
#$yform->setHiddenField('r3nb_date_to', '');

$yform->setValidateField('empty', ['r3nb_title', rex_i18n::msg('addon_r3_newsblogs_side_error_title')]);

$yform->setActionField('db', [rex::getTable('article'), 'id=' . $article_id.' and clang_id='.$clang]);

$yform->setObjectparams('submit_btn_label', rex_i18n::msg('addon_r3_newsblogs_save'));
$form = $yform->getForm();

$form .= '
		<script>
			jQuery(document).ready(function () {
				$(".datepicker").daterangepicker({
					showDropdowns: true,
					singleDatePicker: true,
					"locale": {
						"format": "YYYY-MM-DD",
						"daysOfWeek": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
						"monthNames": ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
						"firstDay": 1
					}
				});
				
				$(".datepicker2").daterangepicker({
					showDropdowns: true,
					singleDatePicker: true,
					"locale": {
						"format": "YYYY-MM-DD",
						"daysOfWeek": ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
						"monthNames": ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
						"firstDay": 1,
						"cancelLabel": "Clear"
					}
				});
				
				$("#rex-yform").on("submit", function(){
					$.pjax.reload("#rex-yform");
				});
				
				jQuery("#yform-formular-r3nb_shortdescription").append(\'<p class="help-block"><small></small></p>\');
				jQuery("#yform-formular-r3nb_shortdescription textarea").bind ("change input keyup keydown keypress mouseup mousedown cut copy paste", function (e) {
					var v = jQuery(this).val().replace(/(\r\n|\n|\r)/gm, "").length;
					jQuery("#yform-formular-r3nb_shortdescription").find("p small").html( v + \' '.$this->i18n('addon_r3_newsblogs_side_info').' \');
					return true;
				}).trigger("keydown");
				
			});
		</script>';

return $form;

?>