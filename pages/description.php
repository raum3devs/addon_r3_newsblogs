<?php
#echo rex_view::title($this->i18n('title'));

$file = rex_file::get(rex_path::addon(rex_package::getName(),'README.md'));
$Parsedown = new Parsedown();

$content =  $Parsedown->text($file);

$fragment = new rex_fragment();
$fragment->setVar('title', $this->i18n('info'), false);
$fragment->setVar('body', $content, false);
echo $fragment->parse('core/page/section.php');


#$content = '';
#$fragment = new rex_fragment();
#$fragment->setVar('title', $this->i18n('less_compiler_erklaerung_title'));
#$fragment->setVar('body', $content, false);
#echo $fragment->parse('core/page/section.php');

?>