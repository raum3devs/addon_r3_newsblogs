<?php

$today = date('Y-m-d');

rex_sql_table::get(rex::getTable('article'))
    ->ensureColumn(new rex_sql_column('r3nb_news', 'tinyint(1)'))
    ->ensureColumn(new rex_sql_column('r3nb_blog', 'tinyint(1)'))
    ->ensureColumn(new rex_sql_column('r3nb_image', 'text'))
    ->ensureColumn(new rex_sql_column('r3nb_title', 'varchar(255)'))
    ->ensureColumn(new rex_sql_column('r3nb_shortdescription', 'text'))
    ->ensureColumn(new rex_sql_column('r3nb_daterange', 'tinyint(1)'))
    ->ensureColumn(new rex_sql_column('r3nb_date_from', 'varchar(255)', FALSE, $today))
    ->ensureColumn(new rex_sql_column('r3nb_date_to', 'varchar(255)', FALSE, $today))
    ->alter()
;


$sql = rex_sql::factory();


//Image Manager Breite für Backend
$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_be'");
$result = $sql->getArray();

if(count($result) == 0){
	$descr = 'Raum3 News/Blogs Addon - Weitenbeschränkung von Bildern der Liste in Backendseite.';
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type')." (status, name, description) VALUES(1, 'r3newsblogs_be', '$descr')");
	
	$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_be'");
	$type_id = $sql->getValue('id');
	
	$createuser = 'R3 News/Blogs Addon';
	$updateuser = $createuser;
	
	$resizeParameters = '{"rex_effect_convert2img":{"rex_effect_convert2img_convert_to":"jpg","rex_effect_convert2img_density":"100"},"rex_effect_crop":{"rex_effect_crop_width":"","rex_effect_crop_height":"","rex_effect_crop_offset_width":"","rex_effect_crop_offset_height":"","rex_effect_crop_hpos":"left","rex_effect_crop_vpos":"top"},"rex_effect_filter_blur":{"rex_effect_filter_blur_repeats":"","rex_effect_filter_blur_type":"","rex_effect_filter_blur_smoothit":""},"rex_effect_filter_colorize":{"rex_effect_filter_colorize_filter_r":"","rex_effect_filter_colorize_filter_g":"","rex_effect_filter_colorize_filter_b":""},"rex_effect_filter_sharpen":{"rex_effect_filter_sharpen_amount":"","rex_effect_filter_sharpen_radius":"","rex_effect_filter_sharpen_threshold":""},"rex_effect_flip":{"rex_effect_flip_flip":"X"},"rex_effect_header":{"rex_effect_header_download":"open_media","rex_effect_header_cache":"no_cache"},"rex_effect_insert_image":{"rex_effect_insert_image_brandimage":"","rex_effect_insert_image_hpos":"left","rex_effect_insert_image_vpos":"top","rex_effect_insert_image_padding_x":"","rex_effect_insert_image_padding_y":""},"rex_effect_mediapath":{"rex_effect_mediapath_mediapath":""},"rex_effect_mirror":{"rex_effect_mirror_height":"","rex_effect_mirror_set_transparent":"colored","rex_effect_mirror_bg_r":"","rex_effect_mirror_bg_g":"","rex_effect_mirror_bg_b":""},"rex_effect_resize":{"rex_effect_resize_width":"80","rex_effect_resize_height":"","rex_effect_resize_style":"maximum","rex_effect_resize_allow_enlarge":"enlarge"},"rex_effect_rounded_corners":{"rex_effect_rounded_corners_topleft":"","rex_effect_rounded_corners_topright":"","rex_effect_rounded_corners_bottomleft":"","rex_effect_rounded_corners_bottomright":""},"rex_effect_workspace":{"rex_effect_workspace_width":"","rex_effect_workspace_height":"","rex_effect_workspace_hpos":"left","rex_effect_workspace_vpos":"top","rex_effect_workspace_set_transparent":"colored","rex_effect_workspace_bg_r":"","rex_effect_workspace_bg_g":"","rex_effect_workspace_bg_b":""}}';
	$cropParameters = '{"rex_effect_convert2img":{"rex_effect_convert2img_convert_to":"jpg","rex_effect_convert2img_density":"100"},"rex_effect_crop":{"rex_effect_crop_width":"80","rex_effect_crop_height":"","rex_effect_crop_offset_width":"","rex_effect_crop_offset_height":"","rex_effect_crop_hpos":"center","rex_effect_crop_vpos":"middle"},"rex_effect_filter_blur":{"rex_effect_filter_blur_repeats":"","rex_effect_filter_blur_type":"","rex_effect_filter_blur_smoothit":""},"rex_effect_filter_colorize":{"rex_effect_filter_colorize_filter_r":"","rex_effect_filter_colorize_filter_g":"","rex_effect_filter_colorize_filter_b":""},"rex_effect_filter_sharpen":{"rex_effect_filter_sharpen_amount":"","rex_effect_filter_sharpen_radius":"","rex_effect_filter_sharpen_threshold":""},"rex_effect_flip":{"rex_effect_flip_flip":"X"},"rex_effect_header":{"rex_effect_header_download":"open_media","rex_effect_header_cache":"no_cache"},"rex_effect_insert_image":{"rex_effect_insert_image_brandimage":"","rex_effect_insert_image_hpos":"left","rex_effect_insert_image_vpos":"top","rex_effect_insert_image_padding_x":"","rex_effect_insert_image_padding_y":""},"rex_effect_mediapath":{"rex_effect_mediapath_mediapath":""},"rex_effect_mirror":{"rex_effect_mirror_height":"","rex_effect_mirror_set_transparent":"colored","rex_effect_mirror_bg_r":"","rex_effect_mirror_bg_g":"","rex_effect_mirror_bg_b":""},"rex_effect_resize":{"rex_effect_resize_width":"","rex_effect_resize_height":"","rex_effect_resize_style":"maximum","rex_effect_resize_allow_enlarge":"enlarge"},"rex_effect_rounded_corners":{"rex_effect_rounded_corners_topleft":"","rex_effect_rounded_corners_topright":"","rex_effect_rounded_corners_bottomleft":"","rex_effect_rounded_corners_bottomright":""},"rex_effect_workspace":{"rex_effect_workspace_width":"","rex_effect_workspace_height":"","rex_effect_workspace_hpos":"left","rex_effect_workspace_vpos":"top","rex_effect_workspace_set_transparent":"colored","rex_effect_workspace_bg_r":"","rex_effect_workspace_bg_g":"","rex_effect_workspace_bg_b":""}}';
	
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type_effect')." (type_id, effect, parameters, priority, updatedate, updateuser, createdate, createuser)  VALUES ($type_id, 'resize', '$resizeParameters', 1, NOW(), '$updateuser', NOW(), '$createuser')");
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type_effect')." (type_id, effect, parameters, priority, updatedate, updateuser, createdate, createuser)  VALUES ($type_id, 'crop', '$cropParameters', 2, NOW(), '$updateuser', NOW(), '$createuser')");
}



//Image Manager Breite für Frontend
$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_fe'");
$result = $sql->getArray();

if(count($result) == 0){
	$descr = 'Raum3 News/Blogs Addon - Weitenbeschränkung von Bildern im Frontend.';
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type')." (status, name, description) VALUES(1, 'r3newsblogs_fe', '$descr')");
	
	$sql->setQuery("SELECT id FROM ".rex::getTable('media_manager_type')." WHERE name = 'r3newsblogs_fe'");
	$type_id = $sql->getValue('id');
	
	$createuser = 'R3 News/Blogs Addon';
	$updateuser = $createuser;
	
	$resizeParameters = '{"rex_effect_convert2img":{"rex_effect_convert2img_convert_to":"jpg","rex_effect_convert2img_density":"100"},"rex_effect_crop":{"rex_effect_crop_width":"","rex_effect_crop_height":"","rex_effect_crop_offset_width":"","rex_effect_crop_offset_height":"","rex_effect_crop_hpos":"left","rex_effect_crop_vpos":"top"},"rex_effect_filter_blur":{"rex_effect_filter_blur_repeats":"","rex_effect_filter_blur_type":"","rex_effect_filter_blur_smoothit":""},"rex_effect_filter_colorize":{"rex_effect_filter_colorize_filter_r":"","rex_effect_filter_colorize_filter_g":"","rex_effect_filter_colorize_filter_b":""},"rex_effect_filter_sharpen":{"rex_effect_filter_sharpen_amount":"","rex_effect_filter_sharpen_radius":"","rex_effect_filter_sharpen_threshold":""},"rex_effect_flip":{"rex_effect_flip_flip":"X"},"rex_effect_header":{"rex_effect_header_download":"open_media","rex_effect_header_cache":"no_cache"},"rex_effect_insert_image":{"rex_effect_insert_image_brandimage":"","rex_effect_insert_image_hpos":"left","rex_effect_insert_image_vpos":"top","rex_effect_insert_image_padding_x":"","rex_effect_insert_image_padding_y":""},"rex_effect_mediapath":{"rex_effect_mediapath_mediapath":""},"rex_effect_mirror":{"rex_effect_mirror_height":"","rex_effect_mirror_set_transparent":"colored","rex_effect_mirror_bg_r":"","rex_effect_mirror_bg_g":"","rex_effect_mirror_bg_b":""},"rex_effect_resize":{"rex_effect_resize_width":"80","rex_effect_resize_height":"","rex_effect_resize_style":"maximum","rex_effect_resize_allow_enlarge":"enlarge"},"rex_effect_rounded_corners":{"rex_effect_rounded_corners_topleft":"","rex_effect_rounded_corners_topright":"","rex_effect_rounded_corners_bottomleft":"","rex_effect_rounded_corners_bottomright":""},"rex_effect_workspace":{"rex_effect_workspace_width":"","rex_effect_workspace_height":"","rex_effect_workspace_hpos":"left","rex_effect_workspace_vpos":"top","rex_effect_workspace_set_transparent":"colored","rex_effect_workspace_bg_r":"","rex_effect_workspace_bg_g":"","rex_effect_workspace_bg_b":""}}';
	$cropParameters = '{"rex_effect_convert2img":{"rex_effect_convert2img_convert_to":"jpg","rex_effect_convert2img_density":"100"},"rex_effect_crop":{"rex_effect_crop_width":"80","rex_effect_crop_height":"","rex_effect_crop_offset_width":"","rex_effect_crop_offset_height":"","rex_effect_crop_hpos":"center","rex_effect_crop_vpos":"middle"},"rex_effect_filter_blur":{"rex_effect_filter_blur_repeats":"","rex_effect_filter_blur_type":"","rex_effect_filter_blur_smoothit":""},"rex_effect_filter_colorize":{"rex_effect_filter_colorize_filter_r":"","rex_effect_filter_colorize_filter_g":"","rex_effect_filter_colorize_filter_b":""},"rex_effect_filter_sharpen":{"rex_effect_filter_sharpen_amount":"","rex_effect_filter_sharpen_radius":"","rex_effect_filter_sharpen_threshold":""},"rex_effect_flip":{"rex_effect_flip_flip":"X"},"rex_effect_header":{"rex_effect_header_download":"open_media","rex_effect_header_cache":"no_cache"},"rex_effect_insert_image":{"rex_effect_insert_image_brandimage":"","rex_effect_insert_image_hpos":"left","rex_effect_insert_image_vpos":"top","rex_effect_insert_image_padding_x":"","rex_effect_insert_image_padding_y":""},"rex_effect_mediapath":{"rex_effect_mediapath_mediapath":""},"rex_effect_mirror":{"rex_effect_mirror_height":"","rex_effect_mirror_set_transparent":"colored","rex_effect_mirror_bg_r":"","rex_effect_mirror_bg_g":"","rex_effect_mirror_bg_b":""},"rex_effect_resize":{"rex_effect_resize_width":"","rex_effect_resize_height":"","rex_effect_resize_style":"maximum","rex_effect_resize_allow_enlarge":"enlarge"},"rex_effect_rounded_corners":{"rex_effect_rounded_corners_topleft":"","rex_effect_rounded_corners_topright":"","rex_effect_rounded_corners_bottomleft":"","rex_effect_rounded_corners_bottomright":""},"rex_effect_workspace":{"rex_effect_workspace_width":"","rex_effect_workspace_height":"","rex_effect_workspace_hpos":"left","rex_effect_workspace_vpos":"top","rex_effect_workspace_set_transparent":"colored","rex_effect_workspace_bg_r":"","rex_effect_workspace_bg_g":"","rex_effect_workspace_bg_b":""}}';
	
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type_effect')." (type_id, effect, parameters, priority, updatedate, updateuser, createdate, createuser)  VALUES ($type_id, 'resize', '$resizeParameters', 1, NOW(), '$updateuser', NOW(), '$createuser')");
	$sql->setQuery("INSERT INTO ".rex::getTable('media_manager_type_effect')." (type_id, effect, parameters, priority, updatedate, updateuser, createdate, createuser)  VALUES ($type_id, 'crop', '$cropParameters', 2, NOW(), '$updateuser', NOW(), '$createuser')");
}


rex_delete_cache();


$this->setConfig('addon_r3_newsblogs_path_news', 'path to news');
$this->setConfig('addon_r3_newsblogs_path_blogs', 'path to blogs');

?>